vim.g.mapleader = " "

-- Undo mapping
vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)

-- File management
vim.keymap.set("n", "<leader>pd", vim.cmd.NvimTreeToggle)
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<leader>pf', telescope.find_files, {})
vim.keymap.set('n', '<leader>pg', telescope.git_files, {})
vim.keymap.set('n', '<leader>ps', function()
  telescope.grep_string({ search = vim.fn.input("Grep: ") })
end)

-- Move lines up and down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Paste over without affecting buffer.
vim.keymap.set("x", "<leader>pp", "\"_dP")

-- Clipboard management
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- Disable Q
vim.keymap.set("n", "Q", "<nop>")

-- Format
vim.keymap.set("n", "<leader>ff", function()
  vim.lsp.buf.format({ async = false, timeout_ms = 5000 })
end)
vim.keymap.set("n", "<leader>ft", "gg=G")

-- Find and replace all occurrences
vim.keymap.set("n", "<leader>rr", ":%s/<c-r><c-w>/<c-r><c-w>/gI<Left><Left><Left>")
vim.keymap.set("v", "<leader>rr", "<Esc>:%s/<c-r><c-w>/<c-r><c-w>/gI<Left><Left><Left>")
vim.keymap.set("n", "<leader>rt", vim.lsp.buf.rename)
vim.keymap.set("v", "<leader>rt", vim.lsp.buf.rename)

-- LSP
vim.keymap.set("n", "<leader>lD", vim.lsp.buf.declaration)
vim.keymap.set("n", "<leader>ld", vim.lsp.buf.definition)
vim.keymap.set("n", "<leader>lh", vim.lsp.buf.hover)
vim.keymap.set("n", "<leader>li", vim.lsp.buf.implementation)
vim.keymap.set("n", "<leader>lc", vim.lsp.buf.code_action)
vim.keymap.set("n", "<leader>lr", vim.lsp.buf.references)

-- Errors
vim.keymap.set("n", "<leader>ee", "<cmd>TroubleToggle<cr>", { noremap = true })
vim.keymap.set("n", "<leader>eq", "<cmd>TroubleToggle quickfix<cr>", { noremap = true })

-- Git
vim.keymap.set('n', '<leader>gd', '<cmd>Gitsigns diffthis<cr>')
vim.keymap.set('n', '<leader>gf', ':DiffviewFileHistory %<cr>')
vim.keymap.set('n', '<leader>gb', ':DiffviewFileHistory<cr>')
vim.keymap.set('n', '<leader>gc', ':DiffviewClose<cr>')

-- Tabs
vim.keymap.set('n', '<leader>th', '<Cmd>BufferPrevious<CR>')
vim.keymap.set('n', '<leader>tl', '<Cmd>BufferNext<CR>')
vim.keymap.set('n', '<leader>tH', '<Cmd>BufferMovePrevious<CR>')
vim.keymap.set('n', '<leader>tL', '<Cmd>BufferMoveNext<CR>')
vim.keymap.set('n', '<leader>tc', '<Cmd>BufferClose<CR>')

-- Integrated Terminal
function _G.set_terminal_keymaps()
  local opts = {buffer = 0}
  vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
  vim.keymap.set('n', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
  vim.keymap.set('n', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
  vim.keymap.set('n', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
  vim.keymap.set('n', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
  vim.keymap.set('n', '<C-w>', [[<C-\><C-n><C-w>]], opts)
end
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
vim.keymap.set('n', '<leader>zz', ':ToggleTerm<cr>')
vim.keymap.set('n', '<leader>za', ':ToggleTerm<cr>')
vim.keymap.set('n', '<leader>zn', ':ToggleTerm direction=horizontal<cr>')
vim.keymap.set('n', '<leader>zt', ':ToggleTerm direction=tab<cr>')
vim.keymap.set('n', '<leader>zh', ':ToggleTerm direction=hover<cr>')

