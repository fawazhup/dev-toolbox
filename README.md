# Personal System Configuration
Configure my personal system for development

## Scope
 - Copy SSH and GPG keys
 - Setup dev containers
 - Install vscode
 - Install and configure nvim?
 
## Gotchas and Disclaimer
 - Only certain bind-mounted directories are allowed.
 - Containers are designed to be long running.
 - Currently there is no desktop and no locale.

## Why is the archived
 - While I do prefer to pick the operting system of my development environment regardless of what I am running on my host there are a few issues that led to me to move to more native solutions.
 - Integrating into the host is very difficult by default.
 - While projects like distrobox or toolbox exist they are not perfect and have gotchas.
 - It is confusing to have two sets of configuration files and setup for the host and the container. For example the terminal emulator needs to live on the host as a gui application.

