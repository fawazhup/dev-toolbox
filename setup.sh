#!/bin/bash

# Copy ssh and gpg
cp -R ssh ~/.ssh
gpg --import private.gpg

# Install podman
sudo dnf install podman
sudo setsebool container_manage_cgroup 1
sudo systemctl disable --now podman.socket
systemctl --user enable --now podman.socket

# Setup dev-toolbox container
podman image build dev-toolbox-container --tag localhost/dev-toolbox
podman container create --name dev-toolbox --pids-limit=-1 --ulimit=host --security-opt "label:disable" -v "/run/user/$(id -u)/podman/podman.sock:/run/podman/podman.sock:z" -v ~/.ssh:/root/.ssh:rw,z -v ~/.gnupg:/root/.gnupg:rw,z -v ~/git:/root/git:rw,z localhost/dev-toolbox

# Setup dev-toolbox service
cp dev-toolbox-container/dev-toolbox.service ~/.config/systemd/user/dev-toolbox.service
systemctl --user enable --now dev-toolbox.service

